<?php
include "../Databaza/DBStorage.php";

$storage = new DBStorage();

    if (isset($_POST['kluc'])) {
        $nadpis = $_POST['nadpis'];
        $text = $_POST['text'];

        if ($_POST['kluc'] == 'pridajNovy') {
            $clanok = new Article($nadpis, $text);
            if($storage->ulozClanok($clanok) == true)
            {
                exit("Článok bol pridaný");
            }
            else
            {
                exit("Článok s takýmto nadpisom aj textom už existuje!");
            }
        }
    }