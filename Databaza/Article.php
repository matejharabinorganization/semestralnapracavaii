<?php

class Article
{
    protected $id;
    protected $nadpis;
    protected $text;

    public function __construct($nadpis = null, $text = null)
    {
        $this->nadpis = $nadpis;
        $this->text = $text;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function setId($pId)
    {
        $this->id = $pId;
    }


    /**
     * @return mixed
     */
    public function getNadpis()
    {
        return $this->nadpis;
    }

    /**
     * @param mixed $nadpis
     */
    public function setNadpis($nadpis)
    {
        $this->nadpis = $nadpis;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

}