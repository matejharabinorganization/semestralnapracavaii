<?php
require "User.php";
require "Article.php";
require "Muzstvo.php";

class DBStorage
{
    private const DB_HOST = 'localhost';
    private const DB_USER = 'root';
    private const DB_PASS = 'dtb456';
    private const DB_NAME = 'prihlasovanie';

    private $databaza;

    public function __construct()
    {
        try {
            $this->databaza = new PDO('mysql:dbname=' .self::DB_NAME. ';host=' .self::DB_HOST,self::DB_USER, self::DB_PASS);
            $this->databaza->setAttribute(PDO::ATTR_ERRMODE,
                PDO::ERRMODE_EXCEPTION);
        } catch(PDOException $e) {
            echo 'Connection failed: ' . $e->getMessage();
        }
    }

    // TODO: toto este uvidim ci mi vobec treba

    function loadAllUsers() {
        $uzivatelia = [];
        $stmt = $this->databaza->query('SELECT * FROM pouzivatelia');
        while($result = $stmt->fetch()) {
            $uzivatel = new User($result['Meno_pouz'], $result['Heslo_pouz']);
            $uzivatelia[] = $uzivatel;
        }
        return $uzivatelia;
    }

    function nacitajLigu($parLiga) {
        $muzstva = [];
        $stmt = $this->databaza->query('SELECT * FROM liga WHERE liga_poradie='.$parLiga.' ');
        while($result = $stmt->fetch()) {
            $muzstvo = new Muzstvo($result['Poradie'], $result['Nazov'], $result['Zapasy'], $result['V'], $result['R'], $result['P'], $result['Body']);
            $muzstva[] = $muzstvo;
        }
        return $muzstva;
    }


    function dajVsetkyZaznamy() {
        $stmt = $this->databaza->prepare("SELECT * FROM clanky");
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $stmt->rowCount();
    }

    function fetchClanky() {
        $query = '';
        $output = array();
        $query .= "SELECT * FROM clanky";

        if(isset($_POST["search"]["value"]))
        {
            $query .= 'WHERE nadpis LIKE "%'.$_POST["search"]["value"].'%"';
            $query .= 'OR text LIKE "%'.$_POST["search"]["value"].'%"';
        }

        if(isset($_POST["order"]))
        {
            $query .= 'ORDER BY '.$_POST['order']['0']['column'].' '.$_POST['order']['0']['dir'].'';
        }
        else
        {
            $query .= 'ORDER BY id ASC ';
        }

        if($_POST["length"] != -1)
        {
            $query .= 'LIMIT' .$_POST['start']. ', ' .$_POST['length'];
        }

        $stmt = $this->databaza->prepare($query);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $data = array();
        $filtered_rows = $stmt->rowCount();

        foreach ($result as $row)
        {
            $sub_array = array();
            $sub_array[] = $row["id"];
            $sub_array[] = $row["nadpis"];
            $sub_array[] = $row["text"];

            $sub_array[] = '<button type="button" name="read" id="'.$row["id"].'" class="btn btn-primary btn-sm view">View</button>';

            $sub_array[] = '<button type="button" name="update" id="'.$row["id"].'" class="btn btn-warning btn-sm view">Edit</button>';

            $sub_array[] = '<button type="button" name="delete" id="'.$row["id"].'" class="btn btn-danger btn-sm view">Delete</button>';

            $data[] = $sub_array;
        }
        $output = array(
            "draw"             => intval($_POST["draw"]),
            "recordsTotal"     => $filtered_rows,
            "recordsFiltered"  => $this->dajVsetkyZaznamy(),
            "data"             => $data
        );
        echo json_encode($output);
    }

   function vytvorPouzivatela(string $prihl_meno, string $heslo): User {
        return new User($prihl_meno, $heslo);
    }

    function overRegistraciuPouzivatela(User $pouzivatel): bool {

        $sql = 'SELECT * FROM pouzivatelia WHERE Meno_pouz=?';
        $stmt = $this->databaza->prepare($sql);
        $stmt->execute([$pouzivatel->getPrihlMeno()]);
        if(($result = $stmt->fetch()))
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    function ulozPouzivatela(User $pouzivatel): bool {
        if($this->overRegistraciuPouzivatela($pouzivatel)) {
            $stmt = $this->databaza->prepare('INSERT INTO pouzivatelia (Meno_pouz, Heslo_pouz) VALUES(?,?)');
            $stmt->execute([$pouzivatel->getPrihlMeno(), $pouzivatel->getHeslo()]);
            return true;
        }
        else
        {
            return false;
        }
    }

    function overPrihlasenie(string $prihl_meno, string $heslo): User {

        $stmt = $this->databaza->prepare('SELECT * FROM pouzivatelia WHERE Meno_pouz=?');
        $stmt->execute([$prihl_meno]);
        $prihlaseny = new User('','');

        while($result = $stmt->fetch())
        {
            if(password_verify($heslo, $result['Heslo_pouz']))
            {
                $prihlaseny = new User($result['Meno_pouz'],$heslo);
                $prihlaseny->setHeslo($result['Heslo_pouz']);
                return $prihlaseny;
            }
        }
        return $prihlaseny;

    }

    // TODO: použiť na články nie na používateľov

    function createArticle(string $nadpis, string $text) {
        return new Article($nadpis, $text);
    }

    function overClanok(Article $clanok): bool {
        $sql = 'SELECT id FROM clanky WHERE nadpis=? AND text=?';
        $stmt = $this->databaza->prepare($sql);
        $stmt->execute([$clanok->getNadpis(), $clanok->getText()]);
        if(($result = $stmt->fetch()))
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    function ulozClanok(Article $clanok): bool {
        if($this->overClanok($clanok)) {
            $stmt = $this->databaza->prepare('INSERT INTO clanky (nadpis, text) VALUES(?,?)');
            $stmt->execute([$clanok->getNadpis(), $clanok->getText()]);
            return true;
        }
        else
        {
            return false;
        }
    }


    public function add($data)
    {

        if (!empty($data)) {
            $fileds = $placholders = [];
            foreach ($data as $field => $value) {
                $fileds[] = $field;
                $placholders[] = ":{$field}";
            }
        }

        $sql = "INSERT INTO players (" . implode(',', $fileds) . ") VALUES (" . implode(',', $placholders) . ")";
        $stmt = $this->databaza->prepare($sql);
        try {
            $this->databaza->beginTransaction();
            $stmt->execute($data);
            $lastInsertedId = $this->databaza->lastInsertId();
            $this->databaza->commit();
            return $lastInsertedId;
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
            $this->databaza->rollback();
        }

    }

    public function update($data, $id)
    {
        if (!empty($data)) {
            $fileds = '';
            $x = 1;
            $filedsCount = count($data);
            foreach ($data as $field => $value) {
                $fileds .= "{$field}=:{$field}";
                if ($x < $filedsCount) {
                    $fileds .= ", ";
                }
                $x++;
            }
        }
        $sql = "UPDATE players SET {$fileds} WHERE id=:id";
        $stmt = $this->databaza->prepare($sql);
        try {
            $this->databaza->beginTransaction();
            $data['id'] = $id;
            $stmt->execute($data);
            $this->databaza->commit();
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
            $this->databaza->rollback();
        }

    }

    /**
     * function is used to get records
     * @param int $stmt
     * @param int @limit
     * @return array $results
     */

    public function getRows($start = 0, $limit = 4)
    {
        $sql = "SELECT * FROM players ORDER BY id DESC LIMIT {$start},{$limit}";
        $stmt = $this->databaza->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        } else {

            $results = [];
        }
        return $results;
    }

    // delete row using id
    public function deleteRow($id)
    {
        $sql = "DELETE FROM players WHERE id=:id";
        $stmt = $this->databaza->prepare($sql);
        try {
            $stmt->execute([':id' => $id]);
            if ($stmt->rowCount() > 0) {
                return true;
            }
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
            return false;
        }

    }

    public function getCount()
    {
        $sql = "SELECT count(*) as pcount FROM players";
        $stmt = $this->databaza->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result['pcount'];
    }
    /**
     * function is used to get single record based on the column value
     * @param string $fileds
     * @param any $value
     * @return array $results
     */
    public function getRow($field, $value)
    {

        $sql = "SELECT * FROM players WHERE {$field}=:{$field}";
        $stmt = $this->databaza->prepare($sql);
        $stmt->execute([":{$field}" => $value]);
        if ($stmt->rowCount() > 0) {
            $result = $stmt->fetch(PDO::FETCH_ASSOC);
        } else {
            $result = [];
        }

        return $result;
    }

    public function searchPlayer($searchText, $start = 0, $limit = 4)
    {
        $sql = "SELECT * FROM players WHERE pname LIKE :search ORDER BY id DESC LIMIT {$start},{$limit}";
        $stmt = $this->databaza->prepare($sql);
        $stmt->execute([':search' => "{$searchText}%"]);
        if ($stmt->rowCount() > 0) {
            $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
        } else {
            $results = [];
        }

        return $results;
    }
    /**
     * funciton is used to upload file
     * @param array $file
     * @return string $newFileName
     */
    /*public function uploadPhoto($file)
    {
        if (!empty($file)) {
            $fileTempPath = $file['tmp_name'];
            $fileName = $file['name'];
            $fileSize = $file['size'];
            $fileType = $file['type'];
            $fileNameCmps = explode('.', $fileName);
            $fileExtension = strtolower(end($fileNameCmps));
            $newFileName = md5(time() . $fileName) . '.' . $fileExtension;
            $allowedExtn = ["jpg", "png", "gif", "jpeg"];
            if (in_array($fileExtension, $allowedExtn)) {
                $uploadFileDir = getcwd() . '/uploads/';
                $destFilePath = $uploadFileDir . $newFileName;
                if (move_uploaded_file($fileTempPath, $destFilePath)) {
                    return $newFileName;
                }
            }

        }
    }*/



}