<?php


class Muzstvo
{
    protected $poradie;
    protected $nazov;
    protected $zapasy;
    protected $V;
    protected $R;
    protected $P;
    protected $body;


    public function __construct($poradie = null, $nazov = null, $zapasy = null, $V = null, $R = null, $P = null, $body = null)
    {
        $this->poradie = $poradie;
        $this->nazov = $nazov;
        $this->zapasy = $zapasy;
        $this->V = $V;
        $this->R = $R;
        $this->P = $P;
        $this->body = $body;

    }

    /**
     * @return mixed|null
     */
    public function getPoradie()
    {
        return $this->poradie;
    }

    /**
     * @param mixed|null $poradie
     */
    public function setPoradie($poradie)
    {
        $this->poradie = $poradie;
    }

    /**
     * @return mixed|null
     */
    public function getNazov()
    {
        return $this->nazov;
    }

    /**
     * @param mixed|null $nazov
     */
    public function setNazov($nazov)
    {
        $this->nazov = $nazov;
    }

    /**
     * @return mixed|null
     */
    public function getZapasy()
    {
        return $this->zapasy;
    }

    /**
     * @param mixed|null $zapasy
     */
    public function setZapasy($zapasy)
    {
        $this->zapasy = $zapasy;
    }

    /**
     * @return mixed|null
     */
    public function getV()
    {
        return $this->V;
    }

    /**
     * @param mixed|null $V
     */
    public function setV($V)
    {
        $this->V = $V;
    }

    /**
     * @return mixed|null
     */
    public function getR()
    {
        return $this->R;
    }

    /**
     * @param mixed|null $R
     */
    public function setR($R)
    {
        $this->R = $R;
    }

    /**
     * @return mixed|null
     */
    public function getP()
    {
        return $this->P;
    }

    /**
     * @param mixed|null $P
     */
    public function setP($P)
    {
        $this->P = $P;
    }

    /**
     * @return mixed|null
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param mixed|null $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

}