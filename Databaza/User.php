<?php

class User
{
    protected $id;
    protected $Meno_pouz;
    protected $Heslo_pouz;

    public function __construct($Meno_pouz = null, $Heslo_pouz = null)
    {
        $this->Meno_pouz = $Meno_pouz;
        $this->Heslo_pouz = password_hash($Heslo_pouz, PASSWORD_DEFAULT);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return mixed
     */
    public function getPrihlMeno()
    {
        return $this->Meno_pouz;
    }

    /**
     * @param mixed $prihl_meno
     */
    public function setPrihlMeno($Meno_pouz)
    {
        $this->Meno_pouz = $Meno_pouz;
    }

    /**
     * @return mixed
     */
    public function getHeslo()
    {
        return $this->Heslo_pouz;
    }

    /**
     * @param mixed $heslo
     */
    public function setHeslo($Heslo_pouz)
    {
        $this->Heslo_pouz = $Heslo_pouz;
    }

}