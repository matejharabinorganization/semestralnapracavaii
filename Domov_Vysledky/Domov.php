<?php include "../Header_footer/header.php";
?>

<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
<br>


<div class="container">
    <div class="row">

        <div class="col-sm-12 col-sm-offset-2 text-right">
            <div class="container">
                <div class="alert alert alert-primary" role="alert">
                    <h4 class="text-primary text-center">Hráči klubu 1.PPC FORTUNA KEŽMAROK</h4>
                </div>
                <div class="alert alert-success text-center message" role="alert">

                </div>
                <?php
                include_once 'form.php';
                include_once 'profile.php';
                ?>
                <div class="row mb-3">
                    <div class="col-3">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#userModal" id="addnewbtn">Pridaj nového <i
                                    class="fa fa-user-circle-o"></i></button>
                    </div>
                    <div class="col-9">
                        <div class="input-group input-group-lg">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon2"><i class="fa fa-search" aria-hidden="true"></i></span>
                            </div>
                            <input type="text" class="form-control" aria-label="Sizing example input"
                                   aria-describedby="inputGroup-sizing-lg" placeholder="Vyhľadaj..." id="searchinput">

                        </div>
                    </div>
                </div>
                <?php
                include_once 'playerstable.php';
                ?>
                <nav id="pagination">
                </nav>
                <input type="hidden" name="currentpage" id="currentpage" value="1">
            </div>
            <div>

                <!-- JS, Popper.js, and jQuery -->
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
                <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
                <script src="js/script.js"></script>
            </div>
            <div id="overlay" style="display:none;">
                <div class="spinner-border text-danger" style="width: 3rem; height: 3rem;"></div>
                <br />
                Loading...
            </div>


        </div>

    </div>
</div>

<br>

<?php include "../Header_footer/footer.php";
?>


