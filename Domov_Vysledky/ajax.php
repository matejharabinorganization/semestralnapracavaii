<?php

$action = $_REQUEST['action'];

if (!empty($action)) {
    include '../Databaza/DBStorage.php';
    //$obj = new Player();
    $storage = new DBStorage();
}

if ($action == 'adduser' && !empty($_POST)) {
    $pname = $_POST['username'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    //$photo = $_FILES['photo'];
    $playerId = (!empty($_POST['userid'])) ? $_POST['userid'] : '';

    // file (photo) upload
    /*$imagename = '';
    if (!empty($photo['name'])) {
        $imagename = $storage->uploadPhoto($photo);
        $playerData = [
            'pname' => $pname,
            'email' => $email,
            'phone' => $phone,
            'photo' => $imagename,
        ];
    } else {*/
        $playerData = [
            'pname' => $pname,
            'email' => $email,
            'phone' => $phone,
        ];
    //}

    if ($playerId) {
        $storage->update($playerData, $playerId);
    } else {
        $playerId = $storage->add($playerData);
    }

    if (!empty($playerId)) {
        $player = $storage->getRow('id', $playerId);
        echo json_encode($player);
        exit();
    }
}







if ($action == "getusers") {

    $page = (!empty($_GET['page'])) ? $_GET['page'] : 1;
    $limit = 4;
    $start = ($page - 1) * $limit;

    $players = $storage->getRows($start, $limit);
    if (!empty($players)) {
        $playerslist = $players;
    }
    else
    {
        $playerslist = [];
    }

    $total = $storage->getCount();
    $playerArr = ['count' => $total, 'players' => $playerslist];
    echo json_encode($playerArr);

    exit();
}








if ($action == "getuser") {
    $playerId = (!empty($_GET['id'])) ? $_GET['id'] : '';

    if (!empty($playerId)) {
        $player = $storage->getRow('id', $playerId);
        echo json_encode($player);

        exit();
    }
}







if ($action == "deleteuser") {
    $playerId = (!empty($_GET['id'])) ? $_GET['id'] : '';

    if (!empty($playerId)) {

        $isDeleted = $storage->deleteRow($playerId);

        if ($isDeleted) {
            $message = ['deleted' => 1];
        } else {
            $message = ['deleted' => 0];
        }
        echo json_encode($message);

        exit();
    }
}







if ($action == 'search') {
    $queryString = (!empty($_GET['searchQuery'])) ? trim($_GET['searchQuery']) : '';
    $results = $storage->searchPlayer($queryString);
    echo json_encode($results);
    exit();
}
