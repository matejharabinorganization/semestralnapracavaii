<div class="footer ">
    <div class="page-footer font-small pt-4">
        <div class="container-fluid text-center text-md-left">
            <div class="row">
                <div class="col-md-6 mt-md-0 mt-3">
                    <h5 class="text-uppercase" style="color: gainsboro">Kontakt</h5>
                    <p id="textKontakt">Nižná brána, s.č. 2250, 060 01 Kežmarok<br>
                        GPS súradnice: N 49.1424394 E 20.429997100000037<br>
                        Osobne:<br>
                        Utorok: 17.00 – 20.00<br>
                        Streda: 17.00 – 20.00<br>
                        Piatok: 17.00 – 20.00<br>
                        Mestská športová hala Vlada Jančeka, herňa – bývalé kino,<br>
                        Ing.Miroslav Harabin<br>
                        Tel. číslo: 0905 844 187<br>
                        E-mail: ppcfortuna@centrum.sk
                    </p>
                </div>

                <div class="col-md-6 mb-md-0 mb-5">
                    <h5 class="text-uppercase" style="color: gainsboro">Sponzor</h5>
                    <ul class="list-unstyled">
                        <li>
                            <img src="https://www.butteland.com/image/logo.png" alt="Butteland s.r.o." class="sponzor">
                        </li>

                        <li>
                            <img src="https://www.pracovne-ponuky.eu/obrazky/spolocnosti/11737.jpg" alt="Bartfeld"
                                 class="sponzor">
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="footer-copyright text-center py-3" style="color: gainsboro">© 2020 Copyright:
            <a href="https://mdbootstrap.com/education/bootstrap/" style="color:Tomato;" target="_blank">
                MDBootstrap.com</a>
        </div>
    </div>
</div>

</body>
</html>
