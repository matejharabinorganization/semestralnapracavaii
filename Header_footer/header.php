<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Title</title>
    <link rel="stylesheet" href="../Dizajn.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

</head>
<body>

<header class="container-fluid">
    <div class="row">
        <div class="col-md-4">
            <img src="../uploads/logo2.png" alt="logo" id="logo">
        </div>

        <div class="col-md-8">
            <p id="nadpis">1.PPC FORTUNA KEŽMAROK</p>
        </div>
    </div>
</header>

<nav class="navbar navbar-expand-md navbar-light bg-info">

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link bg-info text-white" href="../Domov_Vysledky/Domov.php">Domov</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link bg-info text-white" href="../Domov_Vysledky/Vysledky.php">Výsledky</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle text-white" href="#" data-toggle="dropdown"> Tabuľka </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="../Tabulka/1liga.php">1.liga</a>
                    <a class="dropdown-item" href="../Tabulka/2liga.php">2.liga</a>
                    <a class="dropdown-item" href="../Tabulka/3liga.php">3.liga</a>
                    <a class="dropdown-item" href="../Tabulka/5liga.php">5.liga</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle text-white" href="#" data-toggle="dropdown"> Rozpis </a>
                <div class="dropdown-menu ml-auto">
                    <a class="dropdown-item" href="../Rozpis/1liga.php">1.liga</a>
                    <a class="dropdown-item" href="../Rozpis/2liga.php">2.liga</a>
                    <a class="dropdown-item" href="../Rozpis/3liga.php">3.liga</a>
                    <a class="dropdown-item" href="../Rozpis/5liga.php">5.liga</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle text-white" href="#" data-toggle="dropdown"> Rebríček </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="../Rebricek/slovensky.php">Slovenský</a>
                    <a class="dropdown-item" href="../Rebricek/krajsky.php">Krajský</a>
                    <a class="dropdown-item" href="../Rebricek/okresny.php">Okresný</a>
                </div>
            </li>
        </ul>
    </div>


    <a class="btn btn-dark" href="../Prihlasenie/Prihlasenie.php" role="button" id="prihlasenie">Prihlásenie</a>

</nav>

