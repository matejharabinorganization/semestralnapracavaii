<?php include "../Header_footer/header.php";
include "../Databaza/DBStorage.php";
?>

    <br>

    <div class="container">
        <div class="row">
            <div class="col-sm-2">
                <img src="../uploads/banner.jpg" alt="banner" id="banner">
            </div>
            <div class="col-sm-8">
                <div class="table-responsive">
                    <table class="table table-striped ">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">Poradie</th>
                            <th scope="col">Mužstvo</th>
                            <th scope="col">Zápasy</th>
                            <th scope="col">V</th>
                            <th scope="col">R</th>
                            <th scope="col">P</th>
                            <th scope="col">Body</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $storage = new DBStorage();
                        $i = 1;
                        foreach ($storage->nacitajLigu(3) as $muzstvo) : ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $muzstvo->getNazov(); ?></td>
                                <td><?php echo $muzstvo->getZapasy(); ?></td>
                                <td><?php echo $muzstvo->getV(); ?></td>
                                <td><?php echo $muzstvo->getR(); ?></td>
                                <td><?php echo $muzstvo->getP(); ?></td>
                                <td><?php echo $muzstvo->getBody(); ?></td>
                            </tr>
                        <?php $i++;
                        endforeach; ?>
                        </tbody>
                    </table>
                </div>


            </div>
            <div class="col-sm-2">
                <img src="../uploads/banner.jpg" alt="banner" id="banner">
            </div>

        </div>
    </div>

    <br>


<?php include "../Header_footer/footer.php";
?>